; Copyright 2015 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        [       :LNOT: :DEF: __HAL_BOARD_HDR__
        GBLL    __HAL_BOARD_HDR__

; Board configuration struct

                        ^ 0
BoardConfig_DebugUART   # 4 ; Physical address of debug UART

BoardConfig_HALUART     # 4*4 ; Physical addresses of UARTs, in HAL API order
BoardConfig_HALUARTIRQ  # 4 ; 4 bytes of IRQ numbers for HAL UARTs
BoardConfig_DefaultUART # 1 ; Default UART index
BoardConfig_BoardFlags  # 1 ; flags used for PlatformInfo
                        # 2 ; Spare

BoardConfig_HALI2C      # 5*4 ; Physical addresses of I2Cs, in HAL API order
BoardConfig_HALI2CIRQ   # 5 ; 5 bytes of IRQ numbers of HAL I2Cs
BoardConfig_NumI2C      # 1 ; Number of I2Cs to expose via HAL
BoardConfig_VideoI2C    # 1 ; Index of video I2C bus in above tables, 255 for none
                        # 1 ; Spare

BoardConfig_AudioGPIO   # 4 ; GPIOx (AUDIO_POWER_ON for TWL6040), -1 for none

BoardConfig_MixerChans  # 1 ; Mask of disabled mixer channels
BoardConfig_VBC_Flags   # 1 ; VideoBoardConfig flags
BoardConfig_VBC_LCDNum  # 1 ; Number of LCD configs
BoardConfig_NumUART     # 1 ; Number of UARTs to expose via HAL

BoardConfig_VBC_LCDPtr  # 4 ; Pointer to LCD config list
BoardConfig_InitDevices # 4 ; Board-specific version of HAL_InitDevices
BoardConfig_Name        # 20 ; Config name
BoardConfig_MachID      # 4 ; Linux machine ID, as per arch/arm/tools/mach-types
BoardConfig_EndOfRAM    # 4 ; End of SDRAM (filled in at run time)

BoardConfig_Size        # 0

        ASSERT (BoardConfig_Size :AND: 3) = 0

; Linux machine IDs:
MachID_OMAP35xEVM       *       1535
MachID_BeagleBoard      *       1546
MachID_Pandora          *       1761
MachID_OMAP4430SDP      *       2160
MachID_DevKit8000       *       2330
MachID_IGEPv2           *       2344
MachID_TouchBook        *       2393
MachID_PandaBoard       *       2791
MachID_OMAP5430EVM      *       3777
MachID_OMAP5PANDA       *       4070
; there seems to be (not yet) a separate ID for this machine
MachID_IGEP0050         *       MachID_OMAP5PANDA

; Enumerations of the board type, and any minor revisions within that type

                                ^        0
BoardType_OMAP5_IGEPv5          #        1 ; It's a ISEE IGEPv5 board
BoardType_OMAP5_UEVM            #        1 ; It's a OMAP5 UEVM board

; IGEPv5 revision values:

                                ^        0
BoardRevision_IGEPv5_A          #        1 ; Rev. A board

; OMAP5 UEVM revision values:

                                ^        0
BoardRevision_UEVM_A            #        1 ; Rev. A board


; todo - SDRC register settings? function pointers for more flexible setup?
;        (board revision detection, USB PHY, DVI framer, HAL devices, etc.)

; Video configuration structs - these must match up with the definitions in the OMAPVideo driver

                        ^ 0
LCDTimings_PixelRate    # 4 ; Required pixel rate (Hz)
LCDTimings_HSW          # 2
LCDTimings_HFP          # 2
LCDTimings_HBP          # 2
LCDTimings_Width        # 2
LCDTimings_VSW          # 2
LCDTimings_VFP          # 2
LCDTimings_VBP          # 2
LCDTimings_Height       # 2
LCDTimings_SyncPol      # 4 ; vidclist3 syncpol flags
LCDTimings_Size         # 0

                        ^ 0
LCDConfig_DSI_BPP       # 1 ; BPP of DSI output
LCDConfig_DSI_LANES     # 1 ; Number of data lanes of DSI output
LCDConfig_ACBias_Freq   # 1 ; AC bias pin frequency
LCDConfig_LCDType       # 1 ; LCD type
LCDConfig_Flags         # 4 ; Extra flags
LCDConfig_Power         # 4 ; Function ptr to enable/disable power to panel/output path/etc. R0=hal device, R1=brightness (0-65536)
LCDConfig_LCDTimings    # 4 ; Pointer to required timings, if fixed panel
LCDConfig_Max_PixelRate # 4 ; Max pixel rate (Hz), if flexible panel
LCDConfig_Size          # 0


LCDConfig_LCDType_None          * 0
LCDConfig_LCDType_TFT           * 1
LCDConfig_LCDType_STN_Colour    * 2
LCDConfig_LCDType_STN_Mono4     * 3
LCDConfig_LCDType_STN_Mono8     * 4

LCDConfig_Flags_RF              * 1
LCDConfig_Flags_IEO             * 2
LCDConfig_Flags_IPC             * 4
LCDConfig_Flags_Brightness      * 8 ; LCDConfig_Power can be used to control backlight brightness


                                ^ 0
VideoBoardConfig_sys_clk        # 4 ; System clock frequency
VideoBoardConfig_Max_Porch      # 2 ; Max porch value
VideoBoardConfig_Max_Sync       # 2 ; Max sync value
VideoBoardConfig_DMA_Ptr        # 4 ; Pointer to SDMA registers
VideoBoardConfig_DMA_Device     # 4 ; SDMA IRQ number
VideoBoardConfig_DMA_Chans      # 4 ; Mask of available SDMA channels
VideoBoardConfig_Flags          # 1 ; Extra flags
VideoBoardConfig_Num_LCDs       # 1 ; Number of LCD outputs available
                                # 2 ; Padding
VideoBoardConfig_LCD_Configs    # 4 ; Pointer to list of LCD configs
VideoBoardConfig_Size           # 0

VideoBoardConfig_Flags_SVideo           * 1
VideoBoardConfig_Flags_Composite        * 2


        ] ; __HAL_BOARD_HDR__

        END
